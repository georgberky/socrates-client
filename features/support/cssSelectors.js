// @flow

export function select(selector: string): ?HTMLElement {
  const body = document.body;
  return body ? body.querySelector(selector) : null;
}

export function selectAll(selector: string): ?NodeList<HTMLElement> {
  const body = document.body;
  return body ? body.querySelectorAll(selector) : null;
}
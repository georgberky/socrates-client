import {defineSupportCode} from 'cucumber';
import reducers from '../../src/view/reducers/rootReducer';
import {routerReducer} from 'react-router-redux';
import {HistoryFactory} from '../../src/view/store/store';

function SoCraTesWorld() {
  this.history = HistoryFactory.createTestHistory();
  this.allReducers = Object.assign(reducers, {router: routerReducer});
}

defineSupportCode(({setWorldConstructor}) => {
  setWorldConstructor(SoCraTesWorld);
});
# language:en
Feature:
  As an applicant, I want to see a landing page with basic information about SoCraTes, so that I can get an idea what I can
  expect from attending the conference.

  Background:
    Given the application is running

  Scenario:
  Initial page should show the title

    When I open the "home" page
    Then it shows the ".socrates-title" "SoCraTes 2018"

  Scenario:
  Newsletter registration shows success message on success.
    When I open the "home" page
    And enter "Jane Doe" and "jane.doe@example.com" in the newsletter registration form
    And newsletter registration service available
    And click the newsletter registration button
    Then the message "Thank you for caring about Software Craft. You will now receive the SoCraTes newsletter." is shown

  Scenario:
  Newsletter registration shows failure message on fail.
    When I open the "home" page
    And enter "Jane Doe" and "jane.doe@example.com" in the newsletter registration form
    And newsletter registration service unavailable
    And click the newsletter registration button
    Then the message "We actually cannot add you to the list. Please try again later." is shown


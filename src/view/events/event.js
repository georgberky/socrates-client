// @flow
export default class Event {
  static get PLACE_HOLDER() { return 'Event/PLACE_HOLDER'; }
}

export const heldPlace = (arg: string) => ({type: Event.PLACE_HOLDER, arg});
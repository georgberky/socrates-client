// @flow

export default class AuthenticationEvent {

  static get LOGIN_STARTED(): string {
    return 'Event/LOGIN_STARTED';
  }
  static get LOGIN_SUCCESS(): string {
    return 'Event/LOGIN_SUCCESS';
  }
  static get LOGIN_ERROR(): string {
    return 'Event/LOGIN_ERROR';
  }
  static get LOGOUT_SUCCESS(): string {
    return 'Event/LOGOUT_SUCCESS';
  }
}

export const loginStarted = () => ({type: AuthenticationEvent.LOGIN_STARTED});
export const logoutSuccess = () => ({type: AuthenticationEvent.LOGOUT_SUCCESS});
export const loginSuccess =
  (token: string, data: Object) => (
    {type: AuthenticationEvent.LOGIN_SUCCESS, token, data}
  );
export const loginError = () => ({type: AuthenticationEvent.LOGIN_ERROR});

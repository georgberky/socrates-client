import reducer from './authenticationReducer';
import AuthenticationEvent from '../events/authenticationEvents';

describe('authentication reducer', () => {
  describe('on user login success', () => {
    let currentState;
    beforeEach(() => {
      const initialState = {
        token: '',
        comesFrom: '/',
        isAdministrator: false,
        userName: '',
        hasFinished: false
      };
      currentState = reducer.authentication(initialState, {
        type: AuthenticationEvent.LOGIN_SUCCESS,
        token: 'theToken',
        data: {isAdministrator: false, name: 'UserName'}
      });
    });
    it('token is set', () => {
      expect(currentState.token).toEqual('theToken');
    });
    it('login has finished is true', () => {
      expect(currentState.hasFinished).toBe(true);

    });
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false);

    });
    it('UserName is logged in', () => {
      expect(currentState.userName).toEqual('UserName');

    });
    it('comesFrom is still /', () => {
      expect(currentState.comesFrom).toEqual('/');

    });
  });
  describe('on administrator login success', () => {
    let currentState;
    beforeEach(() => {
      const initialState = {
        token: '',
        comesFrom: '/',
        userName: '',
        isAdministrator: false,
        hasFinished: false
      };
      currentState = reducer.authentication(initialState, {
        type: AuthenticationEvent.LOGIN_SUCCESS,
        token: 'theToken',
        data: {isAdministrator: true, name: 'UserName'}
      });
    });
    it('token is set', () => {
      expect(currentState.token).toEqual('theToken');
    });
    it('login has finished is true', () => {
      expect(currentState.hasFinished).toBe(true);

    });
    it('user is an administrator', () => {
      expect(currentState.isAdministrator).toBe(true);

    });
    it('UserName is logged in', () => {
      expect(currentState.userName).toEqual('UserName');

    });
    it('comesFrom is still /', () => {
      expect(currentState.comesFrom).toEqual('/');

    });
  });
  describe('on login started', () => {
    let currentState;
    beforeEach(() => {
      const initialState = {
        token: '',
        comesFrom: '/',
        isAdministrator: false,
        hasFinished: false
      };
      currentState = reducer.authentication(initialState, {type: AuthenticationEvent.LOGIN_STARTED});
    });
    it('token is empty', () => {
      expect(currentState.token).toEqual('');
    });
    it('login has finished is false', () => {
      expect(currentState.hasFinished).toBe(false);

    });
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false);

    });
    it('comesFrom is still /', () => {
      expect(currentState.comesFrom).toEqual('/');

    });
  });
  describe('on login error', () => {
    let currentState;
    beforeEach(() => {
      const initialState = {
        token: '',
        comesFrom: '/',
        isAdministrator: false,
        hasFinished: false
      };
      currentState = reducer.authentication(initialState, {type: AuthenticationEvent.LOGIN_ERROR});
    });
    it('token is empty', () => {
      expect(currentState.token).toEqual('');
    });
    it('login has finished is true', () => {
      expect(currentState.hasFinished).toBe(true);

    });
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false);

    });
    it('comesFrom is still /', () => {
      expect(currentState.comesFrom).toEqual('/');

    });
  });
  describe('on logout success', () => {
    let currentState;
    beforeEach(() => {
      const initialState = {
        token: '',
        comesFrom: '/',
        isAdministrator: false,
        hasFinished: false
      };
      currentState = reducer.authentication(initialState, {type: AuthenticationEvent.LOGIN_STARTED});
    });
    it('token is empty', () => {
      expect(currentState.token).toEqual('');
    });
    it('login has finished is false', () => {
      expect(currentState.hasFinished).toBe(false);

    });
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false);

    });
    it('comesFrom is still /', () => {
      expect(currentState.comesFrom).toEqual('/');

    });
  });
});
// @flow

import AuthenticationEvent from '../events/authenticationEvents';

export type AuthenticationState = {
  token: string,
  isAdministrator: boolean,
  userName: string,
  comesFrom: string,
  hasFinished: boolean,
}

const INITIAL: AuthenticationState = {
  token: '',
  comesFrom: '/',
  userName: 'Guest',
  isAdministrator: false,
  hasFinished: false
};

const authenticationReducer = (state: AuthenticationState = INITIAL, action: any) => {
  switch (action.type) {
    case AuthenticationEvent.LOGIN_SUCCESS:
      return {
        ...state,
        hasFinished: true,
        token: action.token,
        isAdministrator: action.data.isAdministrator,
        userName: action.data.name
      };
    case AuthenticationEvent.LOGIN_STARTED:
    case AuthenticationEvent.LOGOUT_SUCCESS:
      return {...state, isAdministrator: false, hasFinished: false, token: '', userName: 'Guest'};
    case AuthenticationEvent.LOGIN_ERROR:
      return {...state, isAdministrator: false, hasFinished: true, token: '', userName: 'Guest'};
    default:
      return state;
  }
};

export default {authentication: authenticationReducer};

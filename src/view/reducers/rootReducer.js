// @flow

import emptyReducer from './emptyReducer';
import authenticationReducer from './authenticationReducer';

export default Object.assign({}, emptyReducer, authenticationReducer);
// @flow

type State = {};

const INITIAL: State = {};

const emptyReducer = (state: State = INITIAL) => {
  return state;
};

export default {empty: emptyReducer};
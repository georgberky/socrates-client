// @flow

import React from 'react';
import './Header.css';
import logo from '../../assets/img/socrates_logo_2018.png';

export default function Header() {
  return <div id="header" className="jumbotron">
    <header className="container socrates-header">
      <div className="row">
        <div className="col-lg-4 col-sm-12"><img id="logo" src={logo} alt="SoCraTes 2018"/></div>
        <div className="col-lg-8 col-sm-12">
          <h1 className="socrates-title">SoCraTes 2018</h1>
          <h2>8th International Conference for Software Craft and Testing</h2>
          <h2>August 23 - 26, 2018 • Soltau, Germany</h2>
        </div>
      </div>
    </header>
  </div>;
}

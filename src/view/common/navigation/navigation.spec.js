import React from 'react';
import {shallow} from 'enzyme';
import Navigation from './Navigation';
import rootSaga from '../../sagas/rootSaga';
import rootReducer from '../../reducers/rootReducer';
import StoreFactory, {HistoryFactory} from '../../store/store';

describe('(Component) Navigation', () => {
  const store = StoreFactory.createTestStore(rootReducer, rootSaga, HistoryFactory.createTestHistory);
  const wrapper = shallow(<Navigation store={store}/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

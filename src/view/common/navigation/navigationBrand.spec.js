import React from 'react';
import {shallow} from 'enzyme';
import NavigationBrand from './NavigationBrand';

describe('(Component) Navigation brand', () => {
  const wrapper = shallow(<NavigationBrand/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

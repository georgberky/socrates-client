import React from 'react';
import {shallow} from 'enzyme';
import NavigationToggler from './NavigationToggler';

describe('(Component) Navigation toggler', () => {
  const wrapper = shallow(<NavigationToggler target="target"/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

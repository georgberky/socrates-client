// @flow
import React from 'react';
import {connect} from 'react-redux';
import type {AuthenticationState} from '../../reducers/authenticationReducer';
import {logout} from '../../commands/authenticationCommand';
import NavigationBrand from './NavigationBrand';
import NavigationLink from './NavigationLink';
import NavigationToggler from './NavigationToggler';
import DropdownItem from './DropdownItem';
import DropdownToggler from './DropdownToggler';
import {withRouter} from 'react-router';

type Props = {
  state: AuthenticationState,
  logout: () => void
}

function Navigation(props: Props) {
  const isLoggedIn = props.state.token.trim().length > 0;
  const collapsiblePanelId = 'navbarSupportedContent';
  const userDropdown = 'navbarDropdownMenuLink';
  return (
    <div id="navigation">
      <nav className="navbar navbar-expand-md navbar-dark bg-dark">
        <NavigationBrand/>
        <NavigationToggler target={collapsiblePanelId}/>
        <div className="collapse navbar-collapse" id={collapsiblePanelId}>
          <ul className="navbar-nav mr-auto">
            <NavigationLink url="/format" title="Format" iconClass="fas fa-calendar-check"/>
            <NavigationLink url="/location" title="Location" iconClass="fas fa-globe"/>
            <NavigationLink url="/values" title="Values" iconClass="fas fa-gift"/>
            <NavigationLink url="/accessibility" title="Accessibility" iconClass="fab fa-accessible-icon"/>
            <NavigationLink url="/faq" title="FAQ" iconClass="fas fa-question-circle"/>
            <NavigationLink url="/history" title="History" iconClass="fas fa-history"/>
          </ul>
          <div className="navbar-nav navbar-item mr-3 dropdown">
            <DropdownToggler
              target={userDropdown}
              url="#navigation"
              title={props.state.userName}
              iconClass="fas fa-user"
            />
            <div className="dropdown-menu bg-dark dropdown-menu-right" aria-labelledby={userDropdown}>
              <DropdownItem visible ={!isLoggedIn} url="/login" title="Login" iconClass="fas fa-sign-in-alt"/>
              <DropdownItem visible={isLoggedIn} url="#navigation" title="Profile" iconClass="fas fa-user"/>
              <DropdownItem visible={isLoggedIn} url="#navigation" title="Logout" iconClass="fas fa-sign-out-alt"
                onClick={props.logout}/>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {state: {...state.authentication}};
};

const mapDispatchToProps = {
  logout
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navigation));

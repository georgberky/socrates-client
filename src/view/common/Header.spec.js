import React from 'react';
import {shallow} from 'enzyme';
import Header from './Header';

describe('(Component) Header', () => {
  const wrapper = shallow(<Header />);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

import React from 'react';
import {shallow} from 'enzyme';
import FAQ from './FAQ';

describe('(Component) Format', () => {
  const wrapper = shallow(<FAQ/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

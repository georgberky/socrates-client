// @flow

import './faq.css';
import React from 'react';
import FAQItem from './FAQItem';
import faq from './faqItems';


export default function FAQ() {
  return (
    <div id="faq" className="container">
      <div className="row">
        <div className="col-sm-12">
          <div className="page-header"><h1>FAQ</h1></div>
        </div>
      </div>
      <div id="faq-accordion">
        {faq.map((item, index) =>
          <FAQItem
            parentId="faq-accordion"
            title={item.title}
            content={item.content}
            index={index}
            key={`faq-item-${index}`}
          />
        )}
      </div>
    </div>
  );
}


// @flow

import React from 'react';
import './faq.css';

export type Props = {
  parentId: string,
  title: string,
  index: number,
  content: any
}

export default function FAQItem(props: Props) {
  const prefix = `faq-item-${props.index}`;
  return (
    <div className="card border-secondary">
      <div id={'header' + prefix} className="card-header">
        <h4 className="mb-0">
          <button
            className="btn btn-link"
            data-toggle="collapse"
            data-target={'#' + prefix}
            aria-expanded="false"
            aria-controls={prefix}>
            {props.index + 1}. {props.title}
          </button>
        </h4>
      </div>
      <div
        id={prefix}
        className="collapse"
        aria-labelledby={'header' + prefix}
        data-parent={'#' + props.parentId}
      >
        <div className="card-body">
          {props.content}
        </div>
      </div>
    </div>
  );
}

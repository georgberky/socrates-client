import React from 'react';
import {shallow} from 'enzyme';
import FAQItem from './FAQItem';

describe('(Component) Format', () => {
  const wrapper = shallow(<FAQItem/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

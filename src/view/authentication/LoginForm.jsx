// @flow

import React from 'react';
import './authentication.css';

export type Props = {
  email: string,
  hasValidEmail: boolean,
  hasValidPassword: boolean,
  onPasswordChange: (value: string) => void,
  onEmailChange: (value: string) => void,
  onSubmit: () => void,
  password: string,
  showErrorMessage: boolean
}

function AuthenticationFailed() {
  return (
    <div className="row card-error">
      <div className="col-12">
        Log in failed. Email or password are incorrect.
      </div>
    </div>
  );
}

export default function LoginForm(props: Props) {

  const klass = (isValid) => `form-control mb-2 mr-sm-2 ${isValid ? 'is-valid' : 'is-invalid'}`;
  const emailClass = klass(props.hasValidEmail);
  const passwordClass = klass(props.hasValidPassword);

  const onEmailChange = (event: SyntheticEvent<HTMLInputElement>) => {
    props.onEmailChange(event.currentTarget.value);
  };

  const onPasswordChange = (event: SyntheticEvent<HTMLInputElement>) => {
    props.onPasswordChange(event.currentTarget.value);
  };

  const onSubmit = (event: Event) => {
    event.preventDefault();
    props.onSubmit();
  };

  const isEnabled = (): boolean => {
    return props.hasValidEmail && props.hasValidPassword;
  };

  return (
    <div className="container">
      <div className="card border-secondary card-login">
        <div className="card-header">
          <h2>Login</h2>
        </div>
        <div className="card-body">
          <div className="row">
            <div className="col-3 align-self-center">
              <label htmlFor="auth-email">
                Email:
              </label>
            </div>
            <div className="col-9">
              <input id="auth-email" name="email" type="email" className={emailClass} placeholder="Your email" required
                onChange={onEmailChange} value={props.email}/>
            </div>
          </div>
          <div className="row">
            <div className="col-3 align-self-center">
              <label htmlFor="auth-password">
                Password:
              </label>
            </div>
            <div className="col-9">
              <input id="auth-password" name="password" type="password" className={passwordClass} required
                onChange={onPasswordChange} value={props.password}/>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <button id="auth-login" className="btn btn-primary mb-2" disabled={!isEnabled()} onClick={onSubmit}>
                Login
              </button>
            </div>
          </div>
          {props.showErrorMessage ? <AuthenticationFailed/> : null}
        </div>
      </div>
    </div>
  );
}
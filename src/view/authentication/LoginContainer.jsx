// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import {connect} from 'react-redux';
import type {AuthenticationState} from '../reducers/authenticationReducer';

import './authentication.css';
import LoginForm from './LoginForm';
import isValidEmailFormat from '../../utils/isValidEmailFormat';
import {login} from '../commands/authenticationCommand';


export type Props = {
  login: (string, string, string) => void,
  state: AuthenticationState;
}

type State = {
  email: string,
  password: string,
  hasValidEmail: boolean,
  hasValidPassword: boolean
};


export class LoginContainer extends Component<Props, State> {

  static propTypes = {
    login: PropTypes.func.isRequired,
    state: PropTypes.any
  };

  state = {
    email: '',
    password: '',
    hasValidEmail: false,
    hasValidPassword: false
  };


  login = () => {
    this.props.login(this.state.email, this.state.password, this.props.state.comesFrom);
  };

  onEmailChange = (email: string) => {
    const hasValidEmail = email.trim().length > 0 && isValidEmailFormat(email);
    this.setState({...this.state, email, hasValidEmail});
  };

  onPasswordChange = (password: string) => {
    const hasValidPassword = password.trim().length > 0;
    this.setState({...this.state, password, hasValidPassword});
  };

  render = () => {
    const {email, password, hasValidEmail, hasValidPassword} = this.state;
    const showErrorMessage = this.props.state.hasFinished && this.props.state.token.trim().length === 0;
    return (
      <div id="login">
        <LoginForm
          email={email}
          hasValidEmail={hasValidEmail}
          password={password}
          hasValidPassword={hasValidPassword}
          onEmailChange={this.onEmailChange}
          onPasswordChange={this.onPasswordChange}
          onSubmit={this.login}
          showErrorMessage={showErrorMessage}
        />
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {state: {...state.authentication}};
};

const mapDispatchToProps = {
  login
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);

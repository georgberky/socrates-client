// @flow

import axios from 'axios';
import NewsletterFormModel from '../model/NewsletterFormModel';
import {addInterestedPerson} from './api';

jest.mock('axios');

describe('api', () => {

  it('add interested person resolves to true when server returns true', (done) => {
    const promise = Promise.resolve(true);
    axios.post.mockImplementation(() => {
      return promise;
    });
    addInterestedPerson(new NewsletterFormModel('name', 'email@example.com')).then((result) => {
      axios.post.mockRestore();
      expect(result).toBeTruthy();
      done();
    });
  });
  it('add interested person resolves to false when server returns false', (done) => {
    const promise = Promise.resolve(false);
    axios.post.mockImplementation(() => {
      return promise;
    });
    addInterestedPerson(new NewsletterFormModel('name', 'email@example.com')).then((result) => {
      axios.post.mockRestore();
      expect(result).toBeFalsy();
      done();
    });
  });
  it('add interested person rejects when server rejects', (done) => {
    const promise = Promise.reject(new Error('error'));
    axios.post.mockImplementation(() => {
      return promise;
    });
    addInterestedPerson(new NewsletterFormModel('name', 'email@example.com')).catch((result) => {
      axios.post.mockRestore();
      expect(result.message).toEqual('error');
      done();
    });
  });
});
// @flow

import axios from 'axios';
import NewsletterFormModel from '../model/NewsletterFormModel';

const BACKEND_URL = 'server/api/v1/';

export const addInterestedPerson = (data: NewsletterFormModel): Promise<boolean> =>
  axios.post(BACKEND_URL + 'interested-people', data);

export const login = (email: string, password: string): Promise<{token: string, isAdministrator: boolean}> => {
  //axios.post(BACKEND_URL + 'login', {email, password});

  //fake authentication
  return new Promise((resolve) => {
    if (email === 'alex@email.de' && password.trim().length > 0) {
      resolve({
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFsZXhAZW1haWwuZGUiLCJuY' +
        'W1lIjoiQWxleCIsImlzQWRtaW5pc3RyYXRvciI6MCwiaWF0IjoxNTIxNTY3MjUyfQ.QJRC7fzx3s4Khhd_jXRtrbaB-p3wx9lEjP-muNU_2-0',
        isAdministrator: false
      });
    }
    resolve({token: '', isAdministrator: false});
  });
};



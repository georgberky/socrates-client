//@flow
export default class Command {
  static get PLACE_HOLDER() { return 'Command/PLACE_HOLDER'; }
}

export const holdPlace = (arg: string) => ({type: Command.PLACE_HOLDER, arg});
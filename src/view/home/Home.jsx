// @flow

import React from 'react';
import Sponsoring from './sponsoring/Sponsoring';
import PartnerConferences from './partnerConferences/PartnerConferences';
import SocratesDescription from './socratesDescription/SocratesDescription';
import Newsletter from './newsletter/NewsletterContainer';
import './home.css';

const conferences = [{
  name: 'SoCraTes Chile',
  description: 'Santiago de Chile',
  url: 'http://www.socrates-conference.cl/'
}, {
  name: 'SoCraTes Canaries',
  description: '12 - 15 April 2018, Gran Canaria, Spain',
  url: 'http://www.socracan.com'
}, {
  name: 'SoCraTes Italy',
  description: '24 - 26 May 2018, Rimini, Italy',
  url: 'http://www.socrates-conference.it/'
}, {
  name: 'SoCraTes UK',
  description: '7 – 10 June 2018, Dorking, England',
  url: 'http://socratesuk.org/'
}, {
  name: 'Software Craftsmanship Unconference',
  description: 'Atlanta, GA, USA',
  url: 'http://scunconf.com/'
}, {
  name: 'JS CraftCamp',
  description: 'Munich, Germany',
  url: 'http://jscraftcamp.org'
}, {
  name: 'SoCraTes Day Switzerland',
  description: '21 September 2018, Zurich, Switzerland',
  url: 'http://socrates-day.ch/'
}, {
  name: 'SoCraTes Days Linz',
  description: 'Linz, Austria',
  url: 'http://socrates-conference.at/'
}, {
  name: 'SoCraTes France',
  description: 'Rochegude, France',
  url: 'http://socrates-fr.github.io'
}, {
  name: 'SoCraTes BE',
  description: 'La Roche-en-Ardenne, Belgium',
  url: 'http://socratesbe.org'
}, {
  name: 'CodeFreeze',
  description: 'Kiilopää, Finland',
  url: 'http://www.codefreeze.fi/'
}, {
  name: 'SoCraTes Conference Switzerland',
  description: '15 - 18 March 2018, Ftan, Switzerland',
  url: 'http://socrates-ch.org/'
}, {
  name: 'I T.A.K.E. Unconference',
  description: '7 - 8 June 2018, Bucharest, Romania',
  url: 'http://itakeunconf.com/'
}];

export function Home() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-8">
          <div className="row column-left">
            <div className="col-xs-12">
              <SocratesDescription/>
            </div>
          </div>
          <div className="row column-left">
            <div className="col-xs-12">
              <Newsletter/>
            </div>
          </div>
          <div className="row column-left">
            <div className="col-xs-12">
              <PartnerConferences conferences={conferences}/>
            </div>
          </div>
        </div>
        <div className="logocolumn col-sm-4 col-md-3 col-lg-4 col-md-offset-1 col-lg-offset-0">
          <div className="sidebar">
            <Sponsoring/>
          </div>
        </div>
      </div>
    </div>
  );
}

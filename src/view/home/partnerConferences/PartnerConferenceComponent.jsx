// @flow

import React from 'react';

type Props = {
  url: string,
  name: string,
  description: string
}

export default function PartnerConferenceComponent({url, name, description}: Props) {
  return (
    <p>
      <a href={url} target="_blank">{name}</a><br/>
      <small>{description}</small>
    </p>
  );
}

// @flow

export type Sponsor = {
  url: string,
  logo: string,
  name: string,
}
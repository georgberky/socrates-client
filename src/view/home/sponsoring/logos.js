// @flow

import andrena from '../../../assets/logos/andrena160.png';
import codecentric from '../../../assets/logos/codecentric160.png';
import coderbyheart from '../../../assets/logos/coderbyheart160.png';
import codurance from '../../../assets/logos/codurance160.png';
import datev from '../../../assets/logos/datev160.png';
import dpunkt from '../../../assets/logos/dpunkt160.png';
import github from '../../../assets/logos/github160.png';
import holidayCheck from '../../../assets/logos/holidaycheck160.png';
import holisticon from '../../../assets/logos/holisticon160.png';
import improuv from '../../../assets/logos/improuv160.png';
import innoq from '../../../assets/logos/innoq160.png';
import itagile from '../../../assets/logos/it-agile160.png';
import kloseBrothers from '../../../assets/logos/klosebrothers160.png';
import maibornWolff from '../../../assets/logos/maibornwolff160.png';
import mercateo from '../../../assets/logos/mercateo160.png';
import mozaicWorks from '../../../assets/logos/mozaic_works160.png';
import msgGillardon from '../../../assets/logos/msgGillardon160.png';
import novatec from '../../../assets/logos/novatec160.png';
import oose from '../../../assets/logos/oose160.png';
import qixxit from '../../../assets/logos/qixxit160.png';
import seibert from '../../../assets/logos/seibert160.png';
import tng from '../../../assets/logos/tng160.png';
import wikimedia from '../../../assets/logos/wikimedia160.png';

const logos = {
  andrena,
  codecentric,
  coderbyheart,
  codurance,
  datev,
  dpunkt,
  github,
  holidayCheck,
  holisticon,
  improuv,
  innoq,
  itagile,
  kloseBrothers,
  maibornWolff,
  mercateo,
  'mozaic_works': mozaicWorks,
  msgGillardon,
  novatec,
  oose,
  qixxit,
  seibert,
  tng,
  wikimedia
};

export default logos;
import React from 'react';
import NewsletterForm from './NewsletterForm';
import {shallow} from 'enzyme';

describe('(Component) NewsletterForm', () => {
  const wrapper = shallow(<NewsletterForm name="" hasValidName="false" email="" hasValidEmail="false"
    message="" onNameChange="() => {}" onEmailChange="() => {}" onSubmit="() => {}" isDisabled="true"/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

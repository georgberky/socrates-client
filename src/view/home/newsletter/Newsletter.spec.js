import Newsletter from './NewsletterContainer';
import setUpJsDom from '../../../test/setupJsDom';
import {mount} from 'enzyme';
import React from 'react';
import {ADD_SUCCEEDED, ADD_FAILED} from './NewsletterConstants';
import * as api from '../../requests/api';
import {stub} from 'sinon';

describe('NewsletterContainer', () => {
  let newsletter;
  let root;

  beforeEach(() => {
    root = setUpJsDom();
    newsletter = mount(<Newsletter/>, {attachTo: root});
  });

  function setInputValue(selector, value) {
    const wrapper = newsletter.find(selector);
    const node = wrapper.instance();
    node.value = value;
    wrapper.simulate('change', {target: {value}});
    newsletter.update();
  }

  function fillForm(name, email) {
    setInputValue('#newsletter-name', name);
    setInputValue('#newsletter-email', email);
  }
  function fillFormAndClickButton(name, email) {
    fillForm(name, email);
    newsletter.find('#newsletter-form-button').simulate('click');
  }

  it('renders correctly', () => {
    expect(newsletter).toMatchSnapshot();
  });
  it('empty name is invalid', () => {
    expect(newsletter.state('hasValidName')).toBeFalsy();
  });
  it('filled name is valid', () => {
    fillForm('name', '');
    expect(newsletter.state('hasValidName')).toBeTruthy();
  });
  it('empty email is invalid', () => {
    expect(newsletter.state('hasValidEmail')).toBeFalsy();
  });
  it('filled well formatted email is valid', () => {
    fillForm('name', 'valid@example.com');
    expect(newsletter.state('hasValidEmail')).toBeTruthy();
  });
  it('filled with invalid formatted email is invalid', () => {
    fillForm('name', 'invalidExample.com');
    expect(newsletter.state('emailValid')).toBeFalsy();
  });
  it('after a successfully add proper message is shown', () => {
    newsletter.instance().success();
    newsletter.update();
    expect(newsletter.find('#newsletter-message').instance().innerHTML).toEqual(ADD_SUCCEEDED);
  });
  it('after a fail add proper message is shown', () => {
    newsletter.instance().fail();
    newsletter.update();
    expect(newsletter.find('#newsletter-message').instance().innerHTML).toEqual(ADD_FAILED);
  });
  describe('with api', () => {
    afterEach(()=>{
      api.addInterestedPerson.restore();
    });
    it('on call success after button click form is emptied', (done) => {
      const promise = Promise.resolve(true);
      stub(api, 'addInterestedPerson').returns(promise);
      fillFormAndClickButton('name', 'valid@example.com');
      newsletter.update();
      promise.then(() => {
        expect(newsletter.state('name')).toEqual('');
        expect(newsletter.state('email')).toEqual('');
        done();
      });
    });
    it('on call rejection after button click form contains the old values', (done) => {
      const promise = Promise.reject(new Error(''));
      stub(api, 'addInterestedPerson').returns(promise);
      fillFormAndClickButton('name', 'valid@example.com');
      newsletter.update();
      promise.catch(() => {
        expect(newsletter.state('name')).toEqual('name');
        expect(newsletter.state('email')).toEqual('valid@example.com');
        done();
      });
    });
    it('on call failure after button click form contains the old values', (done) => {
      const promise = Promise.resolve(false);
      stub(api, 'addInterestedPerson').returns(promise);
      fillFormAndClickButton('name', 'valid@example.com');
      newsletter.update();
      promise.then(() => {
        expect(newsletter.state('name')).toEqual('name');
        expect(newsletter.state('email')).toEqual('valid@example.com');
        done();
      });
    });
  });
});
// @flow

import React, {Component} from 'react';
import isValidEmailFormat from '../../../utils/isValidEmailFormat';
import {ADD_SUCCEEDED, ADD_FAILED, EMPTY} from './NewsletterConstants';
import NewsletterForm from './NewsletterForm';
import NewsletterFormModel from '../../model/NewsletterFormModel';
import * as api from '../../requests/api';

export type Props = {}

export type State = {
  name: string,
  email: string,
  hasValidName: boolean,
  hasValidEmail: boolean,
  message: string
}

export default class NewsletterContainer extends Component<Props, State> {

  static propTypes = {};

  state = {
    name: '',
    email: '',
    hasValidName: false,
    hasValidEmail: false,
    message: EMPTY
  };

  success = () => {
    this.setState({
      name: '',
      email: '',
      hasValidName: false,
      hasValidEmail: false,
      message: ADD_SUCCEEDED
    });
  };

  fail = () => {
    this.setState({...this.state, message: ADD_FAILED});
  };

  onSubmit = () => {
    this.setState({...this.state, message: ''});
    const model = new NewsletterFormModel(this.state.name, this.state.email);
    api.addInterestedPerson(model).then((result) => {
      if (result) {
        this.success();
      } else {
        this.fail();
      }
    }).catch(() => {
      this.fail();
    });
  };

  onNameChange = (name: string) => {
    const hasValidName = name.length !== 0;
    this.setState({...this.state, name, hasValidName});
  };

  onEmailChange = (email: string) => {
    const hasValidEmail = email.length !== 0 && isValidEmailFormat(email);
    this.setState({...this.state, email, hasValidEmail});
  };

  render() {
    const {name, hasValidName, email, hasValidEmail, message} = this.state;
    const isDisabled = !hasValidEmail || !hasValidName;
    return (
      <div id="newsletter">
        <NewsletterForm
          name={name}
          hasValidName={hasValidName}
          email={email}
          hasValidEmail={hasValidEmail}
          message={message}
          onNameChange={this.onNameChange}
          onEmailChange={this.onEmailChange}
          onSubmit={this.onSubmit}
          isDisabled={isDisabled}
        />
      </div>
    );
  }
}


// @flow

import React from 'react';
import './newsletter.css';

export type Props = {
  name: string,
  email: string,
  hasValidName: boolean,
  hasValidEmail: boolean,
  message: string,
  onNameChange: (value: string) => void,
  onEmailChange: (value: string) => void,
  onSubmit: () => void,
  isDisabled: boolean
}

export default function NewsletterForm(props: Props) {

  const klass = (isValid) => `form-control mb-2 mr-sm-2 ${isValid ? 'is-valid' : 'is-invalid'}`;
  const nameClass = klass(props.hasValidName);
  const emailClass = klass(props.hasValidEmail);

  const onNameChange = (event: SyntheticEvent<HTMLInputElement>) => {
    props.onNameChange(event.currentTarget.value);
  };

  const onEmailChange = (event: SyntheticEvent<HTMLInputElement>) => {
    props.onEmailChange(event.currentTarget.value);
  };

  const onSubmit = (event: Event) => {
    event.preventDefault();
    props.onSubmit();
  };

  const renderMessage = () => {
    if (props.message && props.message !== '') {
      return (<div id="newsletter-message" className="pulse">{props.message}</div>);
    }
    return '';
  };

  return (<div>
    <div className="segment-header">
      <h3>Newsletter</h3>
    </div>
    <div>
      <p>From time to time we send out important information about the SoCraTes.
        If you want to stay informed, let us know by using the form below.</p>
    </div>
    <form className="form-inline">
      <label htmlFor="name" className="sr-only">Name</label>
      <input id="newsletter-name" name="name" type="text" className={nameClass}
        placeholder="Your name" required onChange={onNameChange}
        value={props.name}/>
      <label htmlFor="email" className="sr-only">Email</label>
      <input id="newsletter-email" name="email" type="email" className={emailClass}
        placeholder="Your email" required onChange={onEmailChange}
        value={props.email}/>
      <button id="newsletter-form-button" className="btn btn-primary mb-2"
        disabled={props.isDisabled}
        onClick={onSubmit}>I am interested
      </button>
    </form>
    {renderMessage()}
  </div>);
}

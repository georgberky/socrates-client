// @flow

import React from 'react';
import PropTypes from 'prop-types';

type NameInputProps = {
  validationClass: string,
  value: string,
  onChange: SyntheticInputEvent<HTMLInputElement> => void
}

export function NameInput(props: NameInputProps) {
  const {validationClass, value, onChange} = props;

  return(
    <div className="form-group col">
      <label htmlFor="name" className="col-form-label">Name</label>
      <input
        type="text"
        name="name"
        className={`form-control ${validationClass}`}
        value={value}
        id="name"
        onChange={onChange}
        required
        placeholder="Your name"
        aria-describedby="nameHelp"
      />
      <small id="nameHelp" className="form-text text-muted">We need your name to communicate with you.</small>
    </div>
  );
}

NameInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  validationClass: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
};

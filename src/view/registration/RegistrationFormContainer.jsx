// @flow

import React from 'react';
import debounce from 'debounce';
import {get, post} from '../../services/api';
import {messageTypes, messages} from './registrationMessages';
import isValidEmailFormat from '../../utils/isValidEmailFormat';
import {RegistrationForm} from './RegistrationForm';
import type {RoomTypeOptions} from './RoomTypeOptions';

type Props = {}

type State = {
  diversitySelected: string,
  email: string,
  hasSelectedDiversity: boolean,
  hasSelectedRoomType: boolean,
  hasValidEmail: boolean,
  hasValidName: boolean,
  message: string,
  messageType: string,
  name: string,
  roomTypeOptions: RoomTypeOptions,
  roomTypeSelected: Array<string>,
  wasSubmitted: boolean
}

const initialState = {
  diversitySelected: '',
  email: '',
  hasSelectedDiversity: false,
  hasSelectedRoomType: false,
  hasValidEmail: false,
  hasValidName: false,
  message: messages[messageTypes.EMPTY],
  messageType: messageTypes.EMPTY,
  name: '',
  roomTypeOptions: [],
  roomTypeSelected: [],
  wasSubmitted: false
};

export class RegistrationFormContainer extends React.Component<Props, State> {
  state = initialState;

  componentDidMount() {
    get().then(roomTypeOptions =>
      this.setState({
        ...this.state,
        roomTypeOptions
      })
    );
  }

  onSuccess() {
    this.setState({
      ...this.state,
      ...initialState,
      roomTypeOptions: this.state.roomTypeOptions,
      message: messages[messageTypes.SUCCESS],
      messageType: messageTypes.SUCCESS,
      wasSubmitted: true
    });
  }

  onFail() {
    this.setState({
      ...this.state,
      message: messages[messageTypes.FAIL],
      messageType: messageTypes.FAIL,
      wasSubmitted: true
    });
  }

  isValidName(name: string) {
    return name.trim().length > 0;
  }

  isValidEmail(email: string) {
    return email.trim().length > 0 && isValidEmailFormat(email);
  }

  onNameChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const name = event.target.value;
    this.setState({
      ...this.state,
      name,
      hasValidName: this.isValidName(name)
    });
  };

  onEmailChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const email = event.target.value;
    this.setState({
      ...this.state,
      email,
      hasValidEmail: this.isValidEmail(email)
    });
  };

  onRoomTypeClick = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const index = this.state.roomTypeSelected.indexOf(event.target.value);
    const roomTypeSelected = index !== -1 ?
      [...this.state.roomTypeSelected.slice(0, index), ...this.state.roomTypeSelected.slice(index + 1)] :
      [...this.state.roomTypeSelected, event.target.value];
    this.setState({
      ...this.state,
      hasSelectedRoomType: roomTypeSelected.length > 0,
      roomTypeSelected
    });
  };

  onDiversityChange = (value: string) => {
    const diversitySelected = value;
    this.setState({
      ...this.state,
      diversitySelected,
      hasSelectedDiversity: diversitySelected.length > 0
    });
  };

  onSubmit = (event: SyntheticInputEvent<HTMLInputElement>) => {
    event.preventDefault();
    const {name, email, roomTypeSelected, diversitySelected} = this.state;
    post(name, email, roomTypeSelected, diversitySelected)
      .then(() => this.onSuccess())
      .catch(() => this.onFail());
  };

  render() {
    const isEnabled = this.state.hasValidName &&
      this.state.hasValidEmail &&
      this.state.hasSelectedRoomType &&
      this.state.hasSelectedDiversity;
    const values = {
      diversitySelected: this.state.diversitySelected,
      email: this.state.email,
      name: this.state.name,
      roomTypeSelected: this.state.roomTypeSelected
    };
    const handlers = {
      onEmailChange: this.onEmailChange,
      onNameChange: this.onNameChange,
      onRoomTypeClick: this.onRoomTypeClick,
      onDiversityChange: this.onDiversityChange,
      onSubmit: debounce(this.onSubmit, 1000, true)
    };

    return (
      <RegistrationForm
        handlers={handlers}
        hasValidEmail={this.state.hasValidEmail}
        hasValidName={this.state.hasValidName}
        isEnabled={isEnabled}
        message={this.state.message}
        messageType={this.state.messageType}
        roomTypeOptions={this.state.roomTypeOptions}
        values={values}
        wasSubmitted={this.state.wasSubmitted}
      />
    );
  }
}

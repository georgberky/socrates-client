// @flow

import React from 'react';
import PropTypes from 'prop-types';

type EmailInputProps = {
  validationClass: string,
  value: string,
  onChange: SyntheticInputEvent<HTMLInputElement> => void
}

export function EmailInput(props: EmailInputProps) {
  const {validationClass, value, onChange} = props;

  return(
    <div className="form-group col">
      <label htmlFor="email" className="col-form-label">Email</label>
      <input
        type="email"
        name="email"
        className={`form-control ${validationClass}`}
        value={value}
        id="email"
        onChange={onChange}
        required
        placeholder="Your email"
        aria-describedby="emailHelp"
      />
      <small id="emailHelp" className="form-text text-muted">
        We&#39;ll never share your email with anyone else, but we need it to contact you.
      </small>
    </div>
  );
}

EmailInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  validationClass: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
};

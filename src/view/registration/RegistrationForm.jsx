// @flow

import React from 'react';
import PropTypes from 'prop-types';
import {RoomTypeInput} from './RoomTypeInput';
import {NameInput} from './NameInput';
import {EmailInput} from './EmailInput';
import {DiversityInput} from './DiversityInput';
import type {RoomTypeOptions} from './RoomTypeOptions';

type Handlers = {
  onNameChange: SyntheticInputEvent<HTMLInputElement> => void,
  onEmailChange: SyntheticInputEvent<HTMLInputElement> => void,
  onSubmit: SyntheticInputEvent<HTMLInputElement> => void,
  onRoomTypeClick: SyntheticInputEvent<HTMLInputElement> => void,
  onDiversityChange: string => void
}

type Values = {
  name: string,
  email: string,
  roomTypeSelected: Array<string>,
  diversitySelected: string
}

type RegistrationFormProps = {
  message: string,
  messageType: string,
  handlers: Handlers,
  hasValidName: boolean,
  hasValidEmail: boolean,
  isEnabled: boolean,
  roomTypeOptions: RoomTypeOptions,
  values: Values,
  wasSubmitted: boolean
}

export function RegistrationForm(props: RegistrationFormProps) {
  const validationClassName = isValid => isValid ? 'is-valid' : 'is-invalid';
  const validName = validationClassName(props.hasValidName);
  const validEmail = validationClassName(props.hasValidEmail);
  const {email, diversitySelected, name, roomTypeSelected} = props.values;
  const {onEmailChange, onNameChange, onRoomTypeClick, onDiversityChange, onSubmit} = props.handlers;

  if (props.wasSubmitted) {
    return (
      <div
        id="registration-message"
        className={`alert alert-${props.messageType.toLowerCase()}`}
        role="alert">
        {props.message}
      </div>
    );
  }

  return (
    <form className="form" id="registration-form" onSubmit={onSubmit}>
      <div className="form-row">
        <NameInput validationClass={validName} value={name} onChange={onNameChange}/>
        <EmailInput validationClass={validEmail} value={email} onChange={onEmailChange}/>
        <RoomTypeInput
          roomTypeOptions={props.roomTypeOptions}
          roomTypeSelected={roomTypeSelected}
          onRoomTypeClick={onRoomTypeClick}
        />
        <DiversityInput
          diversitySelected={diversitySelected}
          onDiversityChange={onDiversityChange}
        />
      </div>
      <div className="form-row">
        <button
          className="btn btn-primary"
          disabled={!props.isEnabled}
          id="submit-registration"
          type="submit"
        >
          I really do want to participate!
        </button>
      </div>
    </form>
  );
}

RegistrationForm.propTypes = {
  handlers: PropTypes.object.isRequired,
  hasValidEmail: PropTypes.bool.isRequired,
  hasValidName: PropTypes.bool.isRequired,
  isEnabled: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  messageType: PropTypes.string.isRequired,
  roomTypeOptions: PropTypes.array.isRequired,
  values: PropTypes.object.isRequired,
  wasSubmitted: PropTypes.bool.isRequired
};

import React from 'react';
import {RegistrationFormContainer} from './RegistrationFormContainer';
import {render, mount, shallow} from 'enzyme';
import {stub} from 'sinon';
import * as api from '../../services/api';
import {messages} from './registrationMessages';

describe('(Component) RegistrationFormContainer', () => {
  it('renders without exploding', () => {
    const wrapper = render(<RegistrationFormContainer/>);

    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    const wrapper = shallow(<RegistrationFormContainer/>);

    expect(wrapper).toMatchSnapshot();
  });

  it('when empty name is keyed then field has invalid class', () => {
    const wrapper = mount(<RegistrationFormContainer/>);

    wrapper.find('#name').simulate('change', {target: {value: ''}});

    expect(wrapper.find('#name').hasClass('is-invalid')).toBe(true);
  });

  it('when invalid email is keyed then field has invalid class', () => {
    const wrapper = mount(<RegistrationFormContainer/>);

    wrapper.find('#email').simulate('change', {target: {value: 'invalid'}});

    expect(wrapper.find('#email').hasClass('is-invalid')).toBe(true);
  });

  it('with unfiled form submit button is disabled', () => {
    const wrapper = mount(<RegistrationFormContainer/>);

    expect(wrapper.find('#submit-registration').prop('disabled')).toBeTruthy();
  });

  describe('with promise', () => {
    const ROOM_TYPES = [
      {value: 'single', label:'Single', prices: ['268€']},
      {value: 'junior', label:'Junior (exclusively)', prices: ['234€']}
    ];

    const getRoomTypeOptions = Promise.resolve(ROOM_TYPES);

    beforeAll(() => {
      stub(api, 'get').returns(getRoomTypeOptions);
    });

    afterAll(() => {
      api.get.restore();
    });

    it('with filled form submit button is enabled', () => {
      const wrapper = mount(<RegistrationFormContainer/>);

      return getRoomTypeOptions.then(() => {
        wrapper.find('#name').simulate('change', {target: {value: 'name'}});
        wrapper.find('#email').simulate('change', {target: {value: 'email@example.com'}});
        wrapper.find('#junior').simulate('click', {target: {value: 'junior'}});
        wrapper.find('#single').simulate('click', {target: {value: 'single'}});
        wrapper.find('#yes').simulate('click', {target: {value: 'yes'}});

        expect(wrapper.find('#submit-registration').prop('disabled')).toBeFalsy();
      });
    });
  });

  describe('with api up', () => {
    const ROOM_TYPES = [
      {value: 'single', label:'Single', prices: ['268€']},
      {value: 'junior', label:'Junior (exclusively)', prices: ['234€']}
    ];

    const getRoomTypeOptions = Promise.resolve(ROOM_TYPES);
    const name = 'jane doe';
    const email = 'jane@example.com';
    const roomTypes = ['junior'];
    const diversity = 'yes';

    beforeAll(() => {
      stub(api, 'get').returns(getRoomTypeOptions);
      stub(api, 'post')
        .withArgs(name, email, roomTypes, diversity)
        .returns(Promise.resolve());
    });

    afterAll(() => {
      api.get.restore();
      api.post.restore();
    });

    it('after successful submit it shows success message', () => {
      const wrapper = mount(<RegistrationFormContainer/>);

      return getRoomTypeOptions.then(() => {
        wrapper.find('#name').simulate('change', {target: {value: name}});
        wrapper.find('#email').simulate('change', {target: {value: email}});
        wrapper.find('#junior').simulate('click', {target: {value: roomTypes[0]}});
        wrapper.find('#single').simulate('click', {target: {value: roomTypes[1]}});
        wrapper.find('#single').simulate('click', {target: {value: roomTypes[1]}});
        wrapper.find('#yes').simulate('click', {target: {value: diversity}});
        wrapper.find('form').simulate('submit');
      }).then(() => {
        wrapper.update();
        expect(wrapper.text()).toContain(messages.SUCCESS);
      });
    });
  });

  describe('with api down', () => {
    const ROOM_TYPES = [
      {value: 'single', label:'Single', prices: ['268€']},
      {value: 'junior', label:'Junior (exclusively)', prices: ['234€']}
    ];

    const getRoomTypeOptions = Promise.resolve(ROOM_TYPES);
    const name = 'jane doe';
    const email = 'jane@example.com';
    const roomTypes = ['junior', 'single'];
    const diversity = 'yes';

    beforeAll(() => {
      stub(api, 'get').returns(getRoomTypeOptions);
      stub(api, 'post')
        .withArgs(name, email, roomTypes)
        .returns(Promise.reject());
    });

    afterAll(() => {
      api.get.restore();
      api.post.restore();
    });

    it('after unsuccessful submit it shows fail message and hides submit submit button', () => {
      const wrapper = mount(<RegistrationFormContainer/>);

      return getRoomTypeOptions.then(() => {
        wrapper.find('#name').simulate('change', {target: {value: name}});
        wrapper.find('#email').simulate('change', {target: {value: email}});
        wrapper.find('#junior').simulate('click', {target: {value: roomTypes[0]}});
        wrapper.find('#single').simulate('click', {target: {value: roomTypes[1]}});
        wrapper.find('#yes').simulate('click', {target: {value: diversity}});
        wrapper.find('form').simulate('submit');
      }).then().then(() => {
        wrapper.update();
        expect(wrapper.text()).toContain(messages.FAIL);
      });
    });
  });
});

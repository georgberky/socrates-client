import React from 'react';
import {RegistrationDescription} from './RegistrationDescription';
import {shallow} from 'enzyme';

describe('(Component) RegistrationDescription', () => {
  it('renders without exploding', () => {
    const wrapper = shallow(<RegistrationDescription/>);

    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    const wrapper = shallow(<RegistrationDescription/>);

    expect(wrapper).toMatchSnapshot();
  });
});

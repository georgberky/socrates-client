export const messageTypes = {
  EMPTY: 'EMPTY',
  SUCCESS: 'SUCCESS',
  FAIL: 'FAIL'
};

export const messages = {
  EMPTY: '',
  SUCCESS: 'Thank you for caring about Software Craft. You are now taking part in the lottery for SoCraTes.',
  FAIL: 'Unfortunately the api is down, please try again later.'
};

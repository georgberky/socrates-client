// @flow

export type RoomTypeOptions = Array<{| value: string, label: string, prices: Array<string> |}>

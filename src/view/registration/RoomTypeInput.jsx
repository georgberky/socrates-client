// @flow

import React from 'react';
import PropTypes from 'prop-types';
import type {RoomTypeOptions} from './RoomTypeOptions';

type Props = {
  onRoomTypeClick: SyntheticInputEvent<HTMLInputElement> => void,
  roomTypeOptions: RoomTypeOptions,
  roomTypeSelected: Array<string>
};

export function RoomTypeInput(props: Props) {
  const hasOptions = props.roomTypeOptions.length > 0;

  return !hasOptions ? null : (
    <div className="table-responsive">
      {props.roomTypeOptions.map(({value, label}) => (
        <div key={value} className="radio-inline">
          <label>
            <input
              checked={props.roomTypeSelected.includes(value)}
              id={value}
              name="roomType"
              onClick={props.onRoomTypeClick}
              type="checkbox"
              value={value}
            />
            <b>&nbsp;{label}</b>
          </label>
        </div>
      ))}
    </div>
  );
}

RoomTypeInput.propTypes = {
  onRoomTypeClick: PropTypes.func.isRequired,
  roomTypeOptions: PropTypes.array.isRequired,
  roomTypeSelected: PropTypes.array.isRequired
};

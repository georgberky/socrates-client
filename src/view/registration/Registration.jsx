// @flow

import React from 'react';
import {RegistrationDescription} from './RegistrationDescription';
import {RegistrationFormContainer} from './RegistrationFormContainer';

export function Registration() {
  return (
    <div id="registration" className="container">
      <RegistrationDescription />
      <RegistrationFormContainer />
    </div>
  );
}

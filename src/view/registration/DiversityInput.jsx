// @flow

import React from 'react';
import PropTypes from 'prop-types';

type Props = {
  diversitySelected: string,
  onDiversityChange: string => void
};

type State = {
  isShowingDetailsForOther: boolean,
  detailsForOther: string
};

type PropsForDetails = {
  hasValidValue: boolean,
  onChange: SyntheticInputEvent<HTMLInputElement> => void,
  value: string
};

function DetailsForOtherInput(props: PropsForDetails) {
  const klass = props.hasValidValue ? 'is-valid' : 'is-invalid';

  return (
    <div className="form-group col">
      <label htmlFor="detailsForOther" className="col-form-label">Details</label>
      <input
        type="text"
        name="detailsForOther"
        className={`form-control ${klass}`}
        value={props.value}
        id="detailsForOther"
        onChange={props.onChange}
        required
        placeholder="Details"
      />
    </div>
  );
}

export class DiversityInput extends React.Component<Props, State> {
  state = {
    isShowingDetailsForOther: false,
    detailsForOther: ''
  };

  isValid = (detailsForOther: string) => detailsForOther.trim().length > 0;

  onDiversityChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const isShowingDetailsForOther = value === 'other';
    this.setState({isShowingDetailsForOther});
    if (isShowingDetailsForOther) {
      this.props.onDiversityChange(this.state.detailsForOther);
    } else {
      this.props.onDiversityChange(value);
    }
  };

  onDiversityDetailsChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const detailsForOther = event.target.value;
    this.setState({detailsForOther});
    this.props.onDiversityChange(detailsForOther);
  };

  render() {
    const OPTIONS = [
      {value: 'yes', label: 'Yes'},
      {value: 'no', label: 'No'},
      {value: 'other', label: 'Other'}
    ];
    const isChecked = value =>
      (value === this.props.diversitySelected) || (value === 'other' && this.state.isShowingDetailsForOther);

    return (
      <div>
        <p>
          To increase the diversity of SoCraTes, we have a quota of rooms set aside for underrepresented groups in
          tech and/or SoCraTes.
        </p>
        We consider the following groups underrepresented in that sense:
        <ul>
          <li>Women in tech</li>
          <li>Persons of Color</li>
          <li>LGBTQIA+ persons</li>
          <li>Persons from abroad</li>
          <li>Disabled persons</li>
        </ul>
        Are you a member of any of the groups listed above?

        {OPTIONS.map(({value, label}) => (
          <div key={value} className="radio-inline">
            <label>
              <input
                checked={isChecked(value)}
                id={value}
                name="diversity"
                onClick={this.onDiversityChange}
                type="radio"
                value={value}
                required
              />
              <b>&nbsp;{label}</b>
            </label>
          </div>
        ))}
        {this.state.isShowingDetailsForOther &&
        <DetailsForOtherInput
          onChange={this.onDiversityDetailsChange}
          value={this.state.detailsForOther}
          hasValidValue={this.isValid(this.state.detailsForOther)}
        />
        }
      </div>
    );
  }
}

DiversityInput.propTypes = {
  diversitySelected: PropTypes.string.isRequired,
  onDiversityChange: PropTypes.func.isRequired
};

// @flow

import socratesPicture from '../../assets/img/socrates2012_history_wall.jpg';
import './history.css';
import React from 'react';

export default function History() {
  return (
    <div id="history" className="container">
      <div className="row">
        <div className="col-sm-12">
          <div className="page-header">
            <h1>History</h1>
            <h2>from 2011 to today.</h2>
          </div>
          <p className="extraspace">
            <img className="img-fluid" src={socratesPicture} alt="SoCraTes history wall"/>
            <small className="float-right">Image courtesy of <a href="http://www.softwareleid.de/" target="_blank"
              rel="noopener noreferrer">Christoph Pater</a>
            </small>
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="card card-body bg-light">
            <p>
              Back in 2011, the first &ldquo;Software Craftsmanship and Testing (un)conference&rdquo; took place in
              Germany. Since then, the community and the event has grown larger each year. </p>
            <p>
              Markus G&auml;rtner has written a nice <a href="http://www.shino.de/2014/03/03/socrates-2014/"
                target="_blank" rel="noopener noreferrer">blog post</a> in 2014, which sums up some details why and how
              SoCraTes was established. He graciously allowed us to quote parts of it in the text that follows. </p>
          </div>
          <h3>2010 <small>The Beginning</small></h3>
          <p>
            <a href="https://twitter.com/leiderleider" target="_blank" rel="noopener noreferrer">Andreas
              Leidig</a> and <a href="https://twitter.com/NicoleRauch" target="_blank" rel="noopener noreferrer">Nicole
            Rauch</a> started the whole thing way back in late 2010. They wanted to get a discussion going regarding a
            conference on Software Craftsmanship in Germany. We decided to meet up during <a href="http://www.xpdays.de"
              target="_blank" rel="noopener noreferrer">XP Days Germany</a> in 2010, and see what we could do.
            We quickly
            agreed on an Unconference format, two days, somewhere laid back. Some folks had organized the German Agile
            Coach Camp and <a href="https://twitter.com/Play4Agile" target="_blank"
              rel="noopener noreferrer">Play4Agile</a> in Rückersbach close to Frankfurt. We decided on that spot as
            well,
            and organized everything for 2011. </p>
          <p>Early on, we figured that we would need outside support. That was when we started to reach out to other
            craftspeople, like <a href="https://twitter.com/slagyr" target="_blank" rel="noopener noreferrer">Micah
              Martin</a>, <a href="https://twitter.com/ade_oshineye" target="_blank" rel="noopener noreferrer">Adewale
              Oshineye</a>, <a href="https://twitter.com/SandroMancuso" target="_blank"
            rel="noopener noreferrer"> Sandro Mancuso</a>, and many, many more. We had around 10-20 participants from
            outside Germany with us at the first SoCraTes. All the tales they shared with us on how they were running
            things in London, Israel, Finland, you-name-it engaged us. It felt good to be around so many like-minded
            folks, and receive outside inspiration. </p>
          <div className="year-2011">
            <h3>2011 <small>First year</small></h3>
          </div>
          <p>
            The first SoCraTes - Software Craftsmanship and Testing (un)conference - was a success. We had some track
            sessions back then, and a full day of Open Space. During the Open Space I joined a session that took a look
            on how to spread the community. With all the energy in the place, we put ourselves on a virtual map of
            Germany. That was when I noticed, oh, there are a bunch of other folks around me that come from a similar
            location as I do. That was also when we decided that we needed to keep that spirit going. We talked to folks
            in our neighborhood, and found soon some company for local user groups to keep on spreading the word. </p>
          <div className="year-2012">
            <h3>2012 <small>Second year</small></h3>
          </div>
          <p>One year later, we came back for SoCraTes 2012. Since the first conference we had established 10 user
            groups all over Germany on Software Craftsmanship, all organized under the banner of <a
            href="http://softwerkskammer.org" target="_blank" rel="noopener noreferrer">Softwerkskammer</a>. There was
            one in Hamburg, one in Karlsruhe, one in Munich, one in Nürnberg, one in Berlin, one in Frankfurt, one in
            Dortmund, one in Düsseldorf, and one shared around Münster, Osnabrück, and Bielefeld. We created a timeline
            of events of what had happened in the various local communities since our first get-together.</p>
          <p>We were amazed about the various events, code retreats, user group meetings, and so on.</p>
          <p>We still adhered to reserve space for foreign inspirations at that time. We had 10-20 people from outside
            Germany with us. However, Rückersbach had just 70 beds overall available. With ten local user groups
            potentially joining our Unconference, we faced a serious problem. From each location just around 5 people
            would be able to join. So, with such a large community, we already excluded many potential attendees.</p>
          <p>The format of the conference had shifted though. We had abandoned previously-set track sessions
            all-together. Instead we focused on two full days of Open Space. That provided the freedom necessary. In the
            end, we decided to run the conference again in Rückersbach, but have it organized by a different group of
            people. We explicitly decided to pass over the organizing responsibility to one of the local groups from
            year to year.</p>
          <div className="year-2013">
            <h3>2013 <small>Third year</small></h3>
          </div>
          <p>In 2013, the limited amount of beds became a problem.</p>
          <p>Notwithstanding the size, Rückersbach had an advantage: it was close to Frankfurt airport (about a one hour
            ride by car). That made it easy for people from other countries to attend, since Frankfurt is the largest
            airport in Germany. It would be hard to find such a spot with more beds in such a good position.</p>
          <p>We discussed again what to do about it, and asked the organizers to seek a location that may scale up to
            200 participants.</p>
          <div className="year-2014">
            <h3>2014 <small>Fourth year</small></h3>
          </div>
          <p>This year we switched to a new location. The choice fell on Soltau, a small town conveniently located about
            an hour&#39;s drive away from Hamburg. The hotel gives off a family vibe and has enough rooms and outside
            locations to enable leisure sessions outdoors. It can easily support 200 participants and so provides room
            for growth.</p>
          <p>2014 turned out to be a great year. We had grown significantly, to 150 participants. People loved the new
            location and in general seemed to have a good time. There were two days of Open Space followed by a day of
            workshops.</p>
          <p>The new location set up an intimate feel for the whole event: cozy outside sessions, plenty of
            opportunities for group discussions of varying sizes and a nice bar area enabling quality evening time.
            Discussions tended to last deep into the night or even until sunrise. On the way back to our normal lives
            everybody seemed to promise to meet again at the same place next year.</p>
          <div className="year-2015">
            <h3>2015 <small>Fifth year</small></h3>
          </div>
          <p>This was the second year in Soltau. If possible, the location felt even more inviting this year - it was
            familiar by now. We had 160 participants, a maximum given the conference dates. We faced an unexpected
            situation - the schedule for open space was over 80% full on both days. The energy and desire to contribute
            was heartwarming and inspiring to observe.</p>
          <p>Too many amazing things happend to note them all down, so instead here is a short <a
            href="https://www.youtube.com/watch?v=8XpB30AWE4Y" target="_blank" rel="noopener noreferrer">song</a> that
            shows the SoCraTes spirit. The only thing left to say is: see you next year in Soltau. </p>
          <div className="year-2016">
            <h3>2016 <small>Sixth year</small></h3>
          </div>
          <p>The first year in which we could have the Soltau location exclusively. This pushed the atmosphere to a new
            level of intimacy. We have been almost 240 people attending. This also maxes out the hotel - but we no
            longer want to grow: It is about as big as we want it to be. </p>
          <div className="year-2017">
            <h3>2017 <small>Seventh year</small></h3>
          </div>
          <p>The second time we could have the Soltau location exclusively, and we put in extra effort to make the event
            as welcoming and inclusive as possible, to the largest amount of people we could think of. </p>
          <p>Most of the many new ideas we put in motion, such as having gender-neutral bathrooms, were direct results
            from an intense workshop we ran at the 2016 conference, ensuring that the conference attendees remain at the
            root of what happens every year.</p>
          <p>Resulting from two equally amazing 2017 sessions on how we see ourselves as a community, the conference
            also changed its name: We, along with a large number of local communities from all over Europe, dropped the
            awkward &quot;manship&quot;, to make it obvious that our focus is and remains on the craft itself, and that
            we welcome everyone, from everywhere, regardless of gender.</p>
          <p>The feedback we received after the 2017 conference, not only from those people belonging to marginalized
            groups, encouraged us that our community is as healthy as ever, and frankly the most warm-hearted and open
            environment we can dream of. </p>
        </div>
      </div>
    </div>
  );
}

// @flow

import React from 'react';
import {Link} from 'react-router-dom';
import './values.css';

export default function Values() {
  return (
    <div id="values" className="container">
      <div className="row">
        <div className="col-md-12">
          <div className="page-header"><h1>The Values of SoCraTes</h1></div>
          <div className="card card-body bg-light"><h3>Inclusiveness</h3>
            <p>A primary goal of SoCraTes conference is to be inclusive to the largest
              number of contributors, with the most varied and diverse backgrounds
              possible.
              See <Link to="/accessibility">accessibility</Link> for details about accessibility of the conference and
              venue.</p>
            <h3>Safe Environment</h3>
            <p>As such, we are committed to providing a friendly, safe and
              welcoming environment for all, regardless of gender, sexual orientation,
              programming language, ability, ethnicity, socioeconomic status, and religion
              (or lack thereof).</p></div>
          <p>And so we invite all those who participate in the SoCraTes conference and the
            community surrounding the conference to help us create safe and positive
            experiences for everyone.</p>
          <ul>
            <li><strong>Be welcoming, friendly, and patient.</strong></li>
            <li><strong>Be respectful. </strong>Not all of us will agree with each other all the time, but
              disagreement is no excuse for poor behavior and poor
              manners. We might all experience some frustration now and then, but we cannot allow that frustration
              to turn into a personal attack.
            </li>
            <li><strong>Be aware of the effect your words may have on others. </strong>We are a community of
              professionals, and we conduct ourselves professionally. Be kind to others. Do not insult or put down
              other
              participants. Harassment and other exclusionary behavior aren&#39;t acceptable.
            </li>
          </ul>
          <h3>Scope</h3>
          <p>This code of conduct applies to the SoCraTes conference itself and to all digital spaces that are related
            to the SoCraTes conference,
            such as the wiki and mailing list.</p></div>
      </div>
      <div className="row">
        <div className="col-md-6"><h4>Credits</h4>
          <p>These values are inspired by the following works.</p>
          <ul>
            <li>
              <a href="https://www.djangoproject.com/conduct/" target="_blank" rel="noopener noreferrer">
                Django Code of Conduct
              </a>
            </li>
            <li>
              <a href="http://citizencodeofconduct.org/" target="_blank" rel="noopener noreferrer">
                Citizenship Code of Conduct
              </a>
            </li>
          </ul>
        </div>
        <div className="col-md-6"><h4>Further Questions?</h4>
          <p>Feel invited to contact us via one of the buttons below.</p></div>
      </div>
    </div>
  );
}


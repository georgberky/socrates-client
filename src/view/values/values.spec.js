import React from 'react';
import {shallow} from 'enzyme';
import Values from './Values';

describe('(Component) Format', () => {
  const wrapper = shallow(<Values/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

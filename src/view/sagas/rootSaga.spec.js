// @flow

import SagaTester from 'redux-saga-tester';
import reducers from '../reducers/rootReducer';
import rootSaga from './rootSaga';
import {holdPlace} from '../commands/command';
import Event from '../events/event';
import {combineReducers} from 'redux';
import NewsletterFormModel from '../model/NewsletterFormModel';

describe('rootSaga should', () => {
  const allReducers = combineReducers(reducers);
  const INITIAL_STATE = {placeHolder: {}, newsletter: {personToBeAdded: new NewsletterFormModel()}};
  let sagaTester: SagaTester;
  //sagaTester.start(rootSaga);

  beforeEach(() => {
    sagaTester = new SagaTester({initialState: INITIAL_STATE, allReducers});
    sagaTester.start(rootSaga);
  });

  afterEach(() => {
    sagaTester.reset();
  });

  describe('upon holdPlace()', () => {

    it('dispatch heldPlace()', async () => {
      sagaTester.dispatch(holdPlace('empty'));
      await sagaTester.waitFor(Event.PLACE_HOLDER);
      expect(sagaTester.store.getState().placeHolder).toEqual({});
    });
  });
});
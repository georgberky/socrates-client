// @flow
import Command from '../commands/command';
import {all, put, takeEvery, call} from 'redux-saga/effects';
import {heldPlace} from '../events/event';
import RoutingCommand, {routeTo} from '../commands/routingCommand';
import {push} from 'react-router-redux';
import AuthenticationCommand from '../commands/authenticationCommand';
import {loginError, loginStarted, loginSuccess, logoutSuccess} from '../events/authenticationEvents';
import {login} from '../requests/api';
import * as jwt from 'jsonwebtoken';

export const JSONWebTokenSecret = '$lsRTf!gksTRcDWs';

function* placeHolderSaga(action: any) {
  // insert code that calls server and returns result
  yield put(heldPlace(action.arg));
}

function* routingToSaga(action: any) {
  yield put(push(action.url));
}
export type LoginCommandData = {
  email: string,
  password: string,
  comesFrom: string
}
function* loginSaga(action: LoginCommandData) {
  yield put(loginStarted());
  const result = yield call(login, action.email, action.password);
  if (result.token.trim().length > 0) {
    const data = yield call(jwt.verify, result.token, JSONWebTokenSecret);
    if (data) {
      yield put(loginSuccess(result.token, data));
      yield put(routeTo(action.comesFrom));
    } else {
      yield put(loginError());
    }
  } else {
    yield put(loginError());
  }
}
function* logoutSaga() {
  yield put(logoutSuccess());
}

function* rootSaga(): any {
  yield all([
    takeEvery(Command.PLACE_HOLDER, placeHolderSaga),
    takeEvery(RoutingCommand.ROUTE_TO, routingToSaga),
    takeEvery(AuthenticationCommand.LOGIN, loginSaga),
    takeEvery(AuthenticationCommand.LOGOUT, logoutSaga)
  ]);
}


export default rootSaga;
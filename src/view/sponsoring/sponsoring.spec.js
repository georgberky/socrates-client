import React from 'react';
import {shallow} from 'enzyme';
import Sponsoring from './Sponsoring';

describe('(Component) Sponsoring', () => {
  const wrapper = shallow(<Sponsoring/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

// @flow

import axios from 'axios';
import type {RoomTypeOptions} from '../view/registration/RoomTypeOptions';

const BACKEND_URL = 'server/api/v1';

// eslint-disable-next-line no-unused-vars
export const post = (a: any, b: any, c: any, d: any): Promise<any> =>
  new Promise((res) => setTimeout(() => res(), 1000));

export const get = (): Promise<RoomTypeOptions> =>
  axios.get(`${BACKEND_URL}/applicants/options`).then(x => x.data);

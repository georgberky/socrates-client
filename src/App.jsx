// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import './App.css';
import {connect, Provider} from 'react-redux';
import {Redirect, Route, Router, Switch} from 'react-router-dom';
import {Home} from './view/home/Home';
import Format from './view/format/Format';
import Values from './view/values/Values';
import Location from './view/location/Location';
import Accessibility from './view/accessibility/Accessibility';
import Imprint from './view/imprint/Imprint';
import Sponsoring from './view/sponsoring/Sponsoring';
import ScrollToTop from './view/common/ScrollToTop';
import History from './view/history/History';
import FAQ from './view/faq/FAQ';
import Header from './view/common/Header';
import Navigation from './view/common/navigation/Navigation';
import Footer from './view/common/Footer';
import LoginContainer from './view/authentication/LoginContainer';

export type Props = {
  store: any,
  history: any,
}

export class App extends Component<Props> {
  static propTypes = {
    history: PropTypes.any,
    store: PropTypes.any
  };

  static defaultProps = {};

  render() {
    return (
      <Provider store={this.props.store}>
        <Router history={this.props.history}>
          <ScrollToTop>
            <Switch>
              <Redirect exact from="/" to="/home"/>
              <Route path="/home">
                <div>
                  <Navigation/>
                  <Header/>
                  <Home/>
                  <Footer/>
                </div>
              </Route>
              <Route path="/format">
                <div>
                  <Navigation/>
                  <Format/>
                  <Footer/>
                </div>
              </Route>
              <Route path="/location">
                <div>
                  <Navigation/>
                  <Location/>
                  <Footer/>
                </div>
              </Route>
              <Route path="/values">
                <div>
                  <Navigation/>
                  <Values/>
                  <Footer/>
                </div>
              </Route>
              <Route path="/accessibility">
                <div>
                  <Navigation/>
                  <Accessibility/>
                  <Footer/>
                </div>
              </Route>
              <Route path="/imprint">
                <div>
                  <Navigation/>
                  <Imprint/>
                  <Footer/>
                </div>
              </Route>
              <Route path="/sponsoring">
                <div>
                  <Navigation/>
                  <Sponsoring/>
                  <Footer/>
                </div>
              </Route>
              <Route path="/history">
                <div>
                  <Navigation/>
                  <History/>
                  <Footer/>
                </div>
              </Route>
              <Route path="/faq">
                <div>
                  <Navigation/>
                  <FAQ/>
                  <Footer/>
                </div>
              </Route>
              <Route path="/login">
                <div>
                  <Navigation/>
                  <LoginContainer/>
                  <Footer/>
                </div>
              </Route>
            </Switch>
          </ScrollToTop>
        </Router>
      </Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {...state};
};

export default connect(mapStateToProps)(App);
